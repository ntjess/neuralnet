from typing import Any

from utilitys import fns
from utilitys.fns import dynamicDocstring
from utilitys.params import ParamEditor

from prochelpers import FuncDatasets, funcDatasets
from view import NeuralNetViewer, GuiNet


class NeuralNetControl(ParamEditor):
  viewer: NeuralNetViewer

  def __init__(self, viewer: Any = None, model: Any = None, **kwargs):
    super().__init__(**kwargs)
    self.viewer = viewer
    self.model = model
    self.registerFunc(self.viewer.testData)
    self.registerFunc(self.createNewNet)
    self.registerFunc(self.train)
    self.registerFunc(self.stopTraining)
    self.registerFunc(self.exportNN)
    self.registerFunc(self.importNN)
    fns.setParamTooltips(self.tree)

  @dynamicDocstring(funcDatasets=funcDatasets)
  def createNewNet(self, nodesPerHiddenLayer='7', funcDataset='iris',
                   nSamps=100, sigma=0.1, mean=0.0):
    """
    :param nodesPerHiddenLayer:
      title: Nodes per hidden layer
      helpText: "Number of nodes in each hidden layer. Accepts a list of numbers
       where each number is separated by a space (e.g. '2 3'). Careful, if you choose too
        many (e.g. '150 100'), the app will freeze due to the crazy number of weights"
    :param funcDataset:
      title: Function dataset
      helpText: "How to generate the data. See `prochelpers.FuncDatasets` for the
        list of available options."
      pType: list
      limits:
        {funcDatasets}
      value: None
    :param nSamps: How many samples to generate. Note that this parameter is not
      used for sklearn datasets like iris, digits, etc.
    :param sigma:
      helpText: Sigma of noise added to dataset
      step: 0.1
      limits: [0, 100]
    :param mean: Offset added to dataset
    """
    data, labels = getattr(FuncDatasets, funcDataset)(nSamps=nSamps, sigma=sigma, mean=mean)
    self.viewer.data = data
    self.viewer.labels = labels
    nodesPerLayer = [data.shape[1]]
    nodesPerLayer.extend([int(num) for num in nodesPerHiddenLayer.strip().split(' ')])
    nodesPerLayer.append(labels.shape[1])
    self.viewer.createNewNet(nodesPerLayer)

  def train(self, replicates=1, batchSize=3, learnRate=0.1, smoothWindow=0):
    """
    :param learnRate:
      helpText: Eta for training
      step: 0.1
      limits: [0, 1]
    :param batchSize: Size of mini batches
    :param replicates: How many times to repeat the training dataset
    :param smoothWindow:
      helpText: "Size of smoothing window. Leave as 0 to avoid smoothing. A higher
        number makes sharp edges in the error graphs disappear"
      limits: [0, 1000]
    """
    self.viewer.train(nReplicates=replicates, miniBatchSz=batchSize, learnRate=learnRate,
                      smoothWindow=smoothWindow)

  def stopTraining(self):
    self.viewer.keepTraining = False

  def exportNN(self, fileName='./nnState'):
    """
    :param fileName:
      pType: filepicker
      openFileMode: False
    """
    self.viewer.nn.save(fileName)

  def importNN(self, fileName='./nnState'):
    """
    :param fileName:
      pType: filepicker
      openFileMode: True
    """
    net = GuiNet.load(fileName)
    self.viewer.createNewNet(net)