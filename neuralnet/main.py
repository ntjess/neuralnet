import sys

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore

from app import MainWin

appInst = pg.mkQApp()

win = MainWin()
win.control.expandAllBtn.click()
QtCore.QTimer.singleShot(0, win.showMaximized)
sys.exit(appInst.exec_())