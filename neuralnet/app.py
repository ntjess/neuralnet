from pyqtgraph.Qt import QtWidgets, QtCore
from pyqtgraph.console import ConsoleWidget
from utilitys.fns import makeExceptionsShowDialogs
from utilitys.widgets import EasyWidget

from control import NeuralNetControl
from view import NeuralNetViewer


class MainWin(QtWidgets.QMainWindow):
  def __init__(self, parent=None):
    super().__init__(parent)
    viewer = NeuralNetViewer(self)
    control = NeuralNetControl(viewer)
    allDataLbl = QtWidgets.QLabel('Results on full dataset')
    allDataLbl.setAlignment(QtCore.Qt.AlignVCenter)
    allDataLbl.setAlignment(QtCore.Qt.AlignCenter)
    cfsnLbl = QtWidgets.QLabel('Confusion Matrix')
    cfsnLbl.setAlignment(QtCore.Qt.AlignVCenter)
    cfsnLbl.setAlignment(QtCore.Qt.AlignCenter)
    EasyWidget.buildMainWin([[viewer.nnPltArea, viewer.errPlot],
                      [control.dock.widget(), [allDataLbl, viewer.summaryTbl],
                       [cfsnLbl, viewer.cfsnTbl]]], layout='V', win=self)

    menu = QtWidgets.QMenuBar()
    self.setMenuBar(menu)
    act = menu.addAction('Dev Console')
    self.console = ConsoleWidget(self, namespace={'win': self, 'viewer': viewer, 'control': control})
    self.console.setWindowFlags(QtCore.Qt.Window)
    act.triggered.connect(self.console.show)

    self.viewer = viewer
    self.control = control
    makeExceptionsShowDialogs(self)

  @property
  def nn(self):
    return self.viewer.nn
