import inspect

import numpy as np
from sklearn import datasets
from sklearn.datasets import make_moons
from sklearn.preprocessing import minmax_scale


def idxToClsArray(labels: np.ndarray, nClasses:int=None):
  if nClasses is None:
    nClasses = np.unique(labels).size
  allClsLbls = np.zeros((labels.size, nClasses))
  allClsLbls[np.arange(labels.size, dtype=int), labels.flatten()] = 1
  return allClsLbls

_toyDatasets = [k[5:] for k in vars(datasets)
                           if k.startswith('load_') and 'image' not in k and 'file' not in k]

def _addNoise(data, sigma, mean):
  return data + np.random.random(data.shape)*sigma-sigma/2+mean

def genFromSklearn(datasetName: str) -> (np.ndarray, np.ndarray):
  if datasetName == 'digits':
    data, labels = getattr(datasets, f'load_{datasetName}')(return_X_y=True, n_class=5)
  else:
    data, labels = getattr(datasets, f'load_{datasetName}')(return_X_y=True)
  # Determine if classification or regression
  if datasetName in {'iris', 'digits', 'wine', 'breast_cancer'}:
    nClasses = np.unique(labels).size
    allClsLbls = np.zeros((data.shape[0], nClasses))
    allClsLbls[np.arange(data.shape[0], dtype=int), labels] = 1
    labels = allClsLbls
  data = minmax_scale(data, feature_range=(-1,1))
  def accessor(sigma=0.1, mean=0, **_kwargs): # kwargs collects 'nsamps' arg
    return _addNoise(data, sigma, mean), labels
  return accessor

class FuncDatasets:
  @staticmethod
  def xor(nSamps=1000, sigma=0.1, mean=0):
    data = np.random.randint(0, 2, (nSamps, 2))
    labels = np.bitwise_xor(*data.T)
    labels = idxToClsArray(labels)
    # data = minmax_scale(data, feature_range=(-1,1))
    return _addNoise(data, sigma, mean), labels

  @staticmethod
  def trivial(nSamps=100, sigma=0.1, mean=0):
    data = np.random.random((nSamps,1))*2-1
    labels = (data > 0).astype(int)
    labels = idxToClsArray(labels, 2)
    return _addNoise(data, sigma, mean), labels

  @staticmethod
  def moons(nSamps=100, sigma=0.1, mean=0):
    data, labels = make_moons(nSamps, noise=sigma)
    labels = idxToClsArray(labels, 2)
    return data+mean, labels

for dataset in _toyDatasets:
  setattr(FuncDatasets, dataset, genFromSklearn(dataset))

funcDatasets = [name for name, func in inspect.getmembers(FuncDatasets, inspect.isfunction)]