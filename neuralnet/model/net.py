import pickle
from typing import List, Optional, Sequence, Callable

import numpy as np
from sklearn.metrics import confusion_matrix
import pandas as pd

from model.layer import Layer, sigmoid, d_sigmoid


class Net:
  def __init__(self, nodesPerLayer=None, activation=sigmoid, d_activation=d_sigmoid):
    if nodesPerLayer is None:
      nodesPerLayer = [2, 1]
    nodesPerLayer = [l for l in nodesPerLayer if l > 0]
    numLayers = len(nodesPerLayer)
    self.layers: List[Optional[Layer]] = [None]*numLayers
    self.layers[numLayers-1] = Layer(nodesPerLayer[-1], activation=activation,
                                     d_activation=d_activation)
    np.random.seed(10)

    for ii in range(numLayers-2, -1, -1):
      nextIdx = ii + 1
      curLayer = Layer(nodesPerLayer[ii], self.layers[nextIdx], activation=activation,
                       d_activation=d_activation)
      self.layers[ii] = curLayer
      self.layers[nextIdx].prevLayer = curLayer

    self.input = self.layers[0]
    self.output = self.layers[-1]

  def forwardProp(self, data: np.ndarray):
    self.input.vals[:] = data.reshape(-1,1)
    # Forward prop not used on first layer, since it's the input
    for curLayer in self.layers[1:]:
      curLayer.forwardProp()
    return self.output.vals

  def batchForwardProp(self, data):
    outs = np.empty((data.shape[0], self.output.vals.size))
    for ii in range(data.shape[0]):
      outs[ii, :] = self.forwardProp(data[ii, :]).T
    return outs

  def updateGrad_Wgts(self, batchOut: np.ndarray, batchLabels: np.ndarray, learnRate=0.1):
    self.output.localGrad[:] = np.mean(batchOut - batchLabels, 0, keepdims=True).T
    # No weight update on the last layer since it's output
    for curLayer in reversed(self.layers[:-1]):
      curLayer.updateGrad_Wgts(learnRate)

  def train(self, data: np.ndarray, labels: np.ndarray, learnRate=0.1):
    numSamps = data.shape[0]
    if numSamps != labels.shape[0]:
      raise ValueError(f'Data vs. label size mismatch.')
    outs = self.batchForwardProp(data)
    self.updateGrad_Wgts(outs, labels, learnRate)
    return self.output.localGrad

  def multiBatchTrain(self, data, labels, trainBatchIdxs: Sequence[Sequence[int]]=None,
                      testBatchIdxs: Sequence[Sequence[int]]=None, miniBatchSz=3,
                      shuffle=True, learnRate=0.1, nReplicates=1,
                      callback: Callable=None):
    """
    Trains the displayed neural network in batches.
    :param data: Override data. If `None`, `self.data` is used
    :param labels: Override labels. If `None`, `self.labels` is used
    :param trainBatchIdxs: List of index batches used for training. Each index into
      `trainBatchIdxs` should be a mini-batch of length > 1. If not provided, defaults
      to [[0,1,2], [3,4,5], ..., ] where the size of each minibatch follows the given
      parameter
    :param testBatchIdxs: List of index batches used for testing. Each index into
      `testBatchIdxs` should be a mini-batch of length > 1. If not provided, defaults
      to the reverse of training batches
    :param miniBatchSz: How many samples are in each mini batch
    :param shuffle: Whether to shuffle data and labels before training.
    :param learnRate: Eta for training
    :param nReplicates: How many times to repeat the training dataset
    :param callback: Callback after each training iteration. Receives
      ('training', 'testing') error as keyword inputs.
    """
    nSamps = data.shape[0]
    if trainBatchIdxs is None:
      trainBatchIdxs = list(range(nSamps))
      if nSamps % miniBatchSz != 0:
        numExtraSamps = miniBatchSz - nSamps % miniBatchSz
        trainBatchIdxs.extend(trainBatchIdxs[:numExtraSamps])
      trainBatchIdxs = np.array(trainBatchIdxs).reshape(-1,miniBatchSz)
    if testBatchIdxs is None:
      testBatchIdxs = np.array(trainBatchIdxs[::-1])
    if callback is None:
      callback = lambda **kwargs: None

    trainErr = []
    testErr = []
    for _ in range(nReplicates):
      if shuffle:
        order = np.random.permutation(nSamps)
        data = data[order]
        labels = labels[order]
      for (trainIdxs, testIdxs) in zip(trainBatchIdxs, testBatchIdxs):
        trainData, testData = data[trainIdxs], data[testIdxs]
        trainLbls, testLbls = labels[trainIdxs], labels[testIdxs]
        self.train(trainData, trainLbls, learnRate)
        trainErr.append(np.abs(self.output.localGrad).sum())
        curTestErr = np.abs(testLbls - self.batchForwardProp(testData)).mean(1).sum()
        testErr.append(curTestErr)
        callback(trainErr=trainErr, testErr=testErr)
    return trainErr, testErr

  def printWgts(self):
    for layer in self.layers[:-1]:
      print(layer.wgts)

  def score(self, data, labels):
    out = self.batchForwardProp(data)
    pred = np.argmax(out, 1)
    trueCls = np.argmax(labels, 1)
    return np.sum(pred == trueCls)/pred.shape[0]

  def save(self, fname):
    with open(fname, 'wb') as ofile:
      pickle.dump(self.layers, ofile)

  @classmethod
  def load(cls, fname: str):
    with open(fname, 'rb') as ifile:
      layers = pickle.load(ifile)
      newNet = cls([1,1])
      newNet.layers = layers
      newNet.input = layers[0]
      newNet.output = layers[-1]
    return newNet

  def confusionMat(self, data, labels, pred=None):
    if pred is None:
      pred = self.batchForwardProp(data)
    cfsnMat = confusion_matrix(*[np.argmax(arr, 1) for arr in (labels, pred)])
    nLabels = cfsnMat.shape[0]
    cfsnMat = cfsnMat.T.tolist()
    rows = [f'True {ii}' for ii in range(nLabels)]
    cfsnMat = np.array([rows] + cfsnMat, dtype=object).T
    cols = [''] + [f'Pred {ii}' for ii in range(nLabels)]
    return pd.DataFrame(cfsnMat, columns=cols)