from __future__ import annotations

import numpy as np


def sigmoid(x: np.ndarray) -> np.ndarray:
  return 1/(1+np.exp(-x))

def d_sigmoid(x: np.ndarray) -> np.ndarray:
  sig = sigmoid(x)
  return sig * (1 - sig)

class Layer:
  def __init__(self, nodesInLayer: int, nextLayer: Layer=None, prevLayer: Layer=None,
               activation=sigmoid, d_activation=d_sigmoid):
    self.nextLayer = nextLayer
    self.prevLayer = prevLayer

    self.vals = np.random.random((nodesInLayer, 1))*2-1
    self._wgtSum = np.empty_like(self.vals)
    self.localGrad = np.empty_like(self.vals)

    self.wgts = np.array([[]])
    self.biases = np.zeros_like(self.vals)
    self.activation = activation
    self.d_activation = d_activation

    if nextLayer is not None:
      # Construct weight matrices
      nodesInNextLayer = nextLayer.vals.size
      self.wgts = np.random.random((nodesInNextLayer, nodesInLayer))*2-1

  def forwardProp(self):
    # sigma(wX + b)
    wx_plusB = (self.prevLayer.wgts @ self.prevLayer.vals) + self.biases
    # Assign to all values so shape can't change (instead of 'vals =')
    self.vals[:] = self.activation(wx_plusB)

  def updateGrad_Wgts(self, learnRate: float):
    self.localGrad = self.wgts.T @ self.nextLayer.localGrad * self.d_activation(self.vals)
    d_wgts = self.nextLayer.localGrad @ self.vals.T
    self.wgts -= learnRate * d_wgts
    self.biases -= learnRate * self.localGrad