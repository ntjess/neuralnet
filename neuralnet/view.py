from __future__ import annotations
from typing import Optional, List, Sequence, Union

import numpy as np
import pandas as pd
import pyqtgraph as pg
from pyqtgraph.Qt import QtWidgets, QtGui
from sklearn.metrics import confusion_matrix
from utilitys.widgets import PandasTableModel

from model.net import Net

app = pg.mkQApp()
class NeuralNetViewer(QtWidgets.QMainWindow):
  def __init__(self, parent=None):
    super().__init__(parent)
    self.keepTraining = True
    self.nn: Optional[GuiNet] = None
    self.data: Optional[np.ndarray] = None
    self.labels: Optional[np.ndarray] = None

    self.nnPltArea = pg.PlotWidget()
    self.nnPltArea.plotItem.setTitle('Neural Network Graph')
    for ax in ['left', 'bottom', 'right', 'top']:
      self.nnPltArea.plotItem.hideAxis(ax)

    self.summaryModel = PandasTableModel(pd.Series(index=['data', 'label', 'pred', 'correct'],
                                                   data=[[0],[0],[0],False]), self)
    self.summaryTbl = QtWidgets.QTableView(self)
    self.summaryTbl.setModel(self.summaryModel)

    self.cfsnModel = PandasTableModel(pd.Series(), parent=self)
    self.cfsnTbl = QtWidgets.QTableView(self)
    self.cfsnTbl.setModel(self.cfsnModel)

    # ------
    # Second column
    # -----
    self.errPlot = pg.PlotWidget()
    trainClr = pg.mkPen('g', width=2)
    testClr = pg.mkPen('r', width=2)
    self.errPlot.plotItem.addLegend()
    self.trainErrCurve = pg.PlotCurveItem(pen=trainClr, name='Train')
    self.testErrCurve = pg.PlotCurveItem(pen=testClr, name='Test')
    for curve in self.trainErrCurve, self.testErrCurve:
      self.errPlot.addItem(curve)
    self.errPlot.plotItem.setTitle('Error vs. epoch')

  def _setSummary(self, data, label, pred):
    newDf = self.summaryModel.makeDefaultDfRows(len(data)).reset_index(drop=True)
    for ii, (dat, lab, pr) in enumerate(zip(data, label, pred)):
      newDf.at[ii, 'data'] = dat
      newDf.at[ii, 'label'] = lab
      newDf.at[ii, 'pred'] = pr
      newDf.at[ii, 'correct'] = np.argmax(pr) == np.argmax(lab)
    self.summaryModel.removeDfRows()
    self.summaryModel.addDfRows(newDf)

    cfsnMat = self.nn.confusionMat(data, label, pred)
    cols = cfsnMat.columns
    self.cfsnModel.changeDefaultRows(pd.Series({c: [0] for c in cols}))
    self.cfsnModel.addDfRows(cfsnMat)

  def createNewNet(self, nodesPerLayerOrNet: Union[GuiNet, Sequence[int]]):
    if isinstance(nodesPerLayerOrNet, list):
      net = GuiNet(nodesPerLayerOrNet)
    else:
      net = nodesPerLayerOrNet
    self.nn = net
    self.nnPltArea.clear()
    self.nnPltArea.addItem(self.nn.valScatter)
    for arrow in self.nn.wgtArrows:
      self.nnPltArea.addItem(arrow)
    # self.nn.valScatter.sigClicked.connect(self.nnValClicked)
    # for arr in self.nn.wgtArrows:
    #   arr.setClickable(True, 10)
    #   arr.sigClicked.connect(self.nnWgtClicked)


  def train(self, trainBatchIdxs: Sequence[Sequence[int]]=None,
            testBatchIdxs: Sequence[Sequence[int]]=None,
            miniBatchSz=3,
            data=None,
            labels=None,
            shuffle=True,
            learnRate=0.1,
            nReplicates=1,
            smoothWindow=10):
    """
    Trains the displayed neural network in batches.
    :param trainBatchIdxs: List of index batches used for training. Each index into
      `trainBatchIdxs` should be a mini-batch of length > 1. If not provided, defaults
      to [[0,1,2], [3,4,5], ..., ] where the size of each minibatch follows the given
      parameter
    :param testBatchIdxs: List of index batches used for testing. Each index into
      `testBatchIdxs` should be a mini-batch of length > 1. If not provided, defaults
      to the reverse of training batches
    :param miniBatchSz: How many samples are in each mini batch
    :param data: Override data. If `None`, `self.data` is used
    :param labels: Override labels. If `None`, `self.labels` is used
    :param shuffle: Whether to shuffle data and labels before training.
    :param learnRate: Eta for training
    :param nReplicates: How many times to repeat the training dataset
    :param smoothWindow: Size of smoothing window. Leave as 0 to avoid smoothing.
      limits: [0, 1000]
    """
    if data is None:
      data = self.data
    else:
      self.data = data
    if labels is None:
      labels = self.labels
    else:
      self.labels = labels
    self.keepTraining = True
    # Initialize data
    def callback(**kwargs):
      if not self.keepTraining:
        raise StopIteration
      trainErr = np.array(kwargs['trainErr'])
      testErr = np.array(kwargs['testErr'])
      if smoothWindow > 0:
        kern = np.ones(smoothWindow)/smoothWindow
        trainErr = np.convolve(trainErr, kern, 'valid')
        testErr = np.convolve(testErr, kern, 'valid')
      iters = np.arange(len(testErr))
      self.trainErrCurve.updateData(iters, np.array(trainErr))
      self.testErrCurve.updateData(iters, np.array(testErr))
      self.nnPltArea.prepareGeometryChange()
      self.nnPltArea.getViewBox().invertY(True)
      self.nn.updateClrs()
      app.processEvents()

    try:
      self.nn.multiBatchTrain(
        data=data, labels=labels, trainBatchIdxs=trainBatchIdxs, testBatchIdxs=testBatchIdxs,
      miniBatchSz=miniBatchSz, shuffle=shuffle, learnRate=learnRate, nReplicates=nReplicates,
      callback=callback)
    except StopIteration:
      return

  def testData(self):
    pred = self.nn.batchForwardProp(self.data)
    self._setSummary(self.data.round(2), self.labels.round(2), pred.round(2))

class GuiNet(Net):
  def __init__(self, nodesPerLayer: Sequence[int]):
    nodesPerLayer = [l for l in nodesPerLayer if l > 0]
    super().__init__(nodesPerLayer)
    self.valScatter = pg.ScatterPlotItem()
    self.valScatter.hoverEvent = self.valScatterHoverEvent
    self.wgtArrows: List[pg.PlotCurveItem] = []

    # Keeping a separate list of pens and brushes allows MUCH faster graph
    # updating than if layers were iterated through each time
    self.wgtPens: List[QtGui.QPen] = []
    self.valBrushes: List[QtGui.QPen] = []
    # pyqtgraph has a colormap class that allows interpolation of weight and
    # value colors between a given number of colors. Allows choosing a new
    # color based on the numeric value of weights or values in the neural net
    self.valCmap = pg.ColorMap([0, 1], [[0, 0, 0, 0], [255, 255, 255, 255]])
    self.wgtCmap = pg.ColorMap([-1,1], [[255, 0, 0, 255], [0, 255, 0, 255]])

    # Cache vals and wgts for quicker updates. These are the same values found
    # in the layers, but stacked in one dimension
    self.allVals: List[np.ndarray] = []
    self.allWgts: List[np.ndarray] = []
    self._drawNetwork(nodesPerLayer)

  def _drawNetwork(self, nodesPerLayer):
    # X- and Y-Locations of the neural network nodes in the plot
    xLocs = []
    yLocs = []
    # 'connections' keeps track of to-from line connections
    connections = []
    lastNodeNum = 0
    maxNumNodes = np.max(nodesPerLayer)
    for ii, numNodes in enumerate(nodesPerLayer):
      offset = 0.5*(maxNumNodes-numNodes)
      xLocs.extend(np.array([ii for _ in range(numNodes)]))
      yLocs.extend(np.linspace(offset, maxNumNodes-offset, numNodes))
      if ii < len(nodesPerLayer)-1:
        toNodeStart = lastNodeNum + numNodes
        for curNodeNum in range(lastNodeNum, lastNodeNum + numNodes):
          for toNodeNum in range(toNodeStart, toNodeStart+nodesPerLayer[ii+1]):
            # Build connections between every node in this layer and every node
            # in the next layer. Next layer nodes start at toNodeNum
            connections.append([curNodeNum, toNodeNum])
      lastNodeNum += numNodes
    # Create list of (x,y) positions for the node scatter plots
    pos = [xLocs, yLocs]
    connections = np.array(connections, dtype=int)
    pos = np.array(pos).T
    nLocs = sum(nodesPerLayer)
    # Create a bunch of gray nodes to start
    valBrushes = [pg.mkBrush(color=[128,128,128]) for _ in range(nLocs)]
    # Make input and output layers visually different (squares instead of
    # circles)
    symbols = ['s' for _ in range(nodesPerLayer[0])]
    symbols.extend(['o' for _ in range(sum(nodesPerLayer[1:-1]))])
    symbols.extend(['s' for _ in range(nodesPerLayer[-1])])
    self.valScatter.setData(data=np.arange(nLocs, dtype=int), pos=pos, symbol=symbols, brush=valBrushes, size=50)
    self.valBrushes: List[QtGui.QBrush] = self.valScatter.data['brush']

    # Find out what lines must be drawn by indexing into the adjacency matrix
    # Connections from node 0 to node 2 will grab the (x,y) locations of each
    # node and draw a line between them
    lines = pos[connections].astype(float)
    for ptPair in lines:
      pen = pg.mkPen('w', width=5)
      curLine = pg.PlotCurveItem(ptPair[:,0], ptPair[:,1], pen=pen)
      curLine.hoverEvent = self.wgtArrowHoverFactory(curLine)
      # Make sure all lines are behind nodes
      curLine.setZValue(-100)
      self.wgtArrows.append(curLine)
    self.wgtPens = [arr.opts['pen'] for arr in self.wgtArrows]

    # Index into layers to get references to each weight and value
    self.populateAllWgts_Vals()
    self.updateClrs()

  def populateAllWgts_Vals(self):
    allWgts = []
    allVals = []
    for layer in self.layers:
      for val in layer.vals:
        allVals.append(val)
      if layer.wgts.size == 0: continue
      for wgt in np.nditer(layer.wgts):
        allWgts.append(wgt)
    self.allWgts = allWgts
    self.allVals = allVals

  def updateClrs(self):
    mags = np.abs(self.allWgts)
    mi = mags.min()
    ma = mags.max()
    if abs(ma - mi) < 0.01:
      mags = np.tile(ma, mags.shape)
    else:
      mags = (mags - mi)*3/(ma - mi)
    colors = self.wgtCmap.mapToQColor(self.allWgts)
    for pen, width, color in zip(self.wgtPens, mags, colors):
      pen.setWidthF(width)
      pen.setColor(color)
    colors = [self.valCmap.mapToQColor(val)[0] for val in self.allVals]
    for brush, color in zip(self.valBrushes, colors):
      brush.setColor(color)

  def valScatterHoverEvent(self, ev: QtGui.QHoverEvent):
    if not hasattr(ev, '_scenePos'): return
    pts = self.valScatter.pointsAt(ev.pos())
    if len(pts) > 0:
      pt = pts[-1]
      self.populateAllWgts_Vals()
      data = str(self.allVals[pt.data()].round(2))
    else:
      data = None
    self.valScatter.setToolTip(data)

  def wgtArrowHoverFactory(self, wgtArrow: pg.PlotCurveItem):
    def newHover(ev: QtGui.QHoverEvent):
      if not hasattr(ev, '_scenePos'): return
      if wgtArrow.mouseShape().contains(ev.pos()):
        self.populateAllWgts_Vals()
        wgtIdx = np.array([arr is wgtArrow for arr in self.wgtArrows], bool).argmax()
        newTxt = str(self.allWgts[wgtIdx].round(2))
        data = newTxt
      else:
        data = None
      wgtArrow.setToolTip(data)
    return newHover

  @classmethod
  def load(cls, fname: str):
    ret: GuiNet = super().load(fname)
    ret._drawNetwork([len(l.vals) for l in ret.layers])
    return ret

