# NeuralNet
## A gui visualizer of generic MLP networks

Choose how many hidden layers should be in your network along with the number of nodes in each layer. Once a dataset is selected (and noise is optionally added), the network is created.

Train the network in batches (with batchSize > 1) or on individual samples. Repeat the dataset 'n' times if you don't have enough samples (and don't care about overfitting). Stop training early if you want, or let it run to completion.

When you finish, export the network (or import the results from an older run).

Want to see how the network performs on your dataset? Run the 'Test Data' function to see the classification of every sample and a confusion matrix summarizing the results.

Ideas for improvements? Submit an issue request!

## Installation
The following two lines are all it takes:
```bash
git clone https://gitlab.com/ntjess/neuralnet.git
pip install -e ./neuralnet
```

## Running the app
You can spawn the main window and run that if you're familiar with PyQt5 graphics. Alternatively, you can run the provided `main` script:
```bash
# From '/path/to/repo/neuralnet/'
python neuralnet/main.py
```