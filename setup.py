from setuptools import setup, find_packages

from pathlib import Path

def _mltGlob(curDir, *globs):
  curDir = Path(curDir)
  outFiles = []
  for curGlob in globs:
    outFiles.extend(curDir.glob(curGlob))
  outFiles = [str(f) for f in outFiles]
  return outFiles

here = Path(__file__).parent
reqs = here/'requirements.txt'
required = []
if reqs.exists():
  lines = (here/'requirements.txt').open('r').readlines()
  for line in lines:
    if not line.startswith('#'):
      required.append(line.strip('\n'))

# Get the long description from the README file
if (here/'README.md').exists():
  long_description = (here / 'README.md').read_text(encoding='utf-8')
else:
  long_description = ''

setup(
  name='neuralnet',
  version='0.0.1',
  package_dir={'neuralnet':'neuralnet'},
  packages=find_packages(),
  install_requires=required,
  include_package_data=True,
  url='https://gitlab.com/ntjess/neuralnet',
  download_url='https://gitlab.com/ntjess/neuralnet',
  license='MIT',
  description='Simple neural network visualizer',
  long_description=long_description,
  long_description_content_type='text/markdown',
  author='Nathan Jessurun',
  author_email='njessurun@ufl.edu',
  classifiers=[
    'Development Status :: 3 - Alpha',      # Chose either "3 - Alpha", "4 - Beta" or "5 - Production/Stable" as the current state of your package
    'Intended Audience :: Developers',      # Define that your audience are developers
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3'
  ],
  python_requires='>=3.7.*',
)
